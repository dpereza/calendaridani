import React from 'react';
import Titol from './Titol';
import Quadricula from "./Quadricula";
import Taula from "./Taula";
import Calendari from "./Calendari"

// import "./App.css";
import "./Calendari.css";


// const MOTOS = [
//   {
//     marca: "honda",
//     model: "scoopy"
//   },
//   {
//     marca: "honda",
//     model: "cbr125"
//   },
//   {
//     marca: "yamaha",
//     model: "fzr1000"
//   },
//   {
//     marca: "gilera",
//     model: "nexus"
//   },
//   {
//     marca: "aprilia",
//     model: "mana"
//   },
// ]


function App() {
  return (
    <>
      <Titol nom="Daniel Pérez" salutacio="Benvingut al calendari de " />
      {/* <Quadricula files={5} columnes={7} /> */}
      {/* <Taula dades={MOTOS} /> */}
      <Calendari mes={4} any={2021} />
    </>
  );
}

export default App;
