import React from "react";

import "./App.css";

const DIES = ["Dl","Dm","Dc","Dj","Dv","Ds","Dg"];

function Dia({valor, festiu}){
    return <div style={{color: festiu ? "red" : "black"}}className="dia">{valor}</div>
}

function Setmana({dies}) {
    return <div className="setmana">{dies.map((el, idx) => <Dia key={idx} festiu={idx ==6 || idx ==5} valor={el} />)}</div>
}

function Calendari({mes, any}) {

    const primerDia=new Date(any,mes-1,1);
    const ultimDia=new Date(any,mes,0);
    const diaSetmana = primerDia.getDay();
    const diesTotals = ultimDia.getDate();

    let llistaDies = [];
    for (let index = 1; index < diaSetmana; index++) {
        llistaDies.push(" ");
    }
    for (let index = 0; index < diesTotals; index++) {
        llistaDies.push(index+1);
    }

    let numSetmanes = Math.ceil(llistaDies.length/7);
    let setmanes = [];
    for (let index = 0; index < diesTotals; index++){
        let dies = llistaDies.splice(0,7);
        setmanes.push (<Setmana dies={dies} />)
    }
   
    
    return <>
    <div class="contenidor">
    <Setmana dies={DIES} />
    {setmanes}
    </div>
    </>
}


export default Calendari;