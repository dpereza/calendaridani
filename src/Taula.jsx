import React from 'react';
import {Table} from 'reactstrap';


function Taula(props){

    const files = props.dades.map(moto => {
        const fila = (
            <tr>
                <td>{moto.marca}</td>
                <td>{moto.model}</td>
            </tr>
        );
        return fila;
    })


return (
    <Table>
        <thead>
            <tr>
                <th>Marca</th>
                <th>Model</th>
            </tr>
        </thead>
        <tbody>
            {files} 
        </tbody>
    </Table>
);

}


export default Taula;